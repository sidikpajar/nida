<?php 
  include("components/header.php");
?>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
      <?php include("components/header-top.php") ?>     
      <?php include("components/ui-setting.php") ?>     
        <div class="app-main">
          <?php include("components/menu-sidebar.php") ?>     
            <div class="app-main__outer">
              <!-- main content -->
              <div class="app-main__inner">
                <!-- Title configurasi -->
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon">
                                <i class="pe-7s-car icon-gradient bg-mean-fruit">
                                </i>
                            </div>
                            <div>Sistem Informasi Pelaporan Perjalanan Dinas Direktorat Angkutan Udara
                                <div class="page-title-subheading">Dashboard ini berisi data yang dapat dilihat sesuai dengan hak akses pengguna yang login.
                                </div>
                            </div>
                        </div>
                        <!-- No Page title action -->  
                    </div>
                </div>            
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-card mb-3 card">
                            <div class="card-header">
                              Daftar laporan <a style="margin-left:20px;" href="laporan-add.php" id="PopoverCustomT-4" class="btn btn-primary btn-sm">Tambah Laporan</a>
                            </div>
                            <div class="table-responsive">
                                <table id="example" class="display" class="align-middle mb-0 table table-borderless table-striped table-hover">
                                    <thead>
                                      <tr>
                                          <th>Kategori</th>
                                          <th>Title</th>
                                          <th>Log Laporan</th>
                                          <th>Actions</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                      $user_id = $_SESSION['user_id'];
                                      $sql="SELECT 
                                                c.category_id AS category_id,
                                                c.name AS category_name,
                                                l.laporan_id AS laporan_id,
                                                l.user_id AS user_id,
                                                l.title AS title 
                                            FROM laporan l
                                            INNER JOIN category c 
                                              ON c.category_id = l.category_id
                                            where user_id='$user_id' ";
                                      $query = mysqli_query($connect,$sql);
                                      while($row = mysqli_fetch_array($query)) {
                                        $laporan_id = $row['laporan_id'];
                                        $category_id = $row['category_id'];
                                      ?>
                                      <tr>
                                        <td>
                                          <?php 
                                            echo $row['category_name'];
                                          ?>
                                        </td>
                                        <td><?php echo $row['title']; ?></td>
                                        <td>
                                        <ol>
                                          <?php
                                            $logs = "SELECT 
                                                  u.name AS NAMAUSER,
                                                  u.user_level AS LEVELUSER,
                                                  r.read_date AS TANGGALDIBACA
                                              FROM read_logs r
                                              INNER JOIN users u
                                                ON u.user_id = r.user_id
                                              where laporan_id='$laporan_id'
                                            ";
                                            $query_logs = mysqli_query($connect,$logs);
                                            while($row_logs = mysqli_fetch_array($query_logs)) {
                                            ?>
                                            
                                            <li>
                                              <div class="badge badge-success mr-1 ml-0">
                                                  <small><?php echo $row_logs['LEVELUSER'];?></small>
                                              </div>
                                              <b style="color:#3f6ad8;"><?php echo $row_logs['NAMAUSER'];?></b> telah membaca tanggal <b><?php echo $row_logs['TANGGALDIBACA'];?></b></li>
                                            <?php } ?>
                                          </ol>
                                        </td>
                                        <td>
                                            <a href="laporan-detail.php?laporan_id=<?php echo $laporan_id  ?>" id="PopoverCustomT-4" class="btn btn-primary btn-sm">Details</a>
                                        </td>
                                      </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="d-block text-center card-footer">
                                
                            </div>
                        </div>
                    </div>
                </div>



                

              </div>
              <?php include("components/footer.php") ?>
            </div>
          <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        </div>
    </div>
  
<script type="text/javascript" src="../core-themes/assets/scripts/main.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body>
</html>
