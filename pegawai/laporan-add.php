<?php 
  include("components/header.php");
?>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
      <?php include("components/header-top.php") ?>     
      <?php include("components/ui-setting.php") ?>     
        <div class="app-main">
          <?php include("components/menu-sidebar.php") ?>     
            <div class="app-main__outer">
              <!-- main content -->
              <div class="app-main__inner">
                <!-- Title configurasi -->
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon">
                                <i class="pe-7s-car icon-gradient bg-mean-fruit">
                                </i>
                            </div>
                            <div>Sistem Informasi Pelaporan Perjalanan Dinas Direktorat Angkutan Udara
                                <div class="page-title-subheading">Dashboard ini berisi data yang dapat dilihat sesuai dengan hak akses pengguna yang login.
                                </div>
                            </div>
                        </div>
                        <!-- No Page title action -->  
                    </div>
                </div>            
                <div class="row">
                    <div class="col-md-6">
                        <div class="main-card mb-3 card">
                           
                            <div class="card-header">Tambah laporan
                            </div>
                            <form class="" role="form" method="POST" action="laporan-add.php" enctype="multipart/form-data">
                              <div class="col-md-12" style="padding:20px;">
                                    <div class="position-relative form-group"><label>Judul Laporan</label><input name="title" placeholder="Masukkan judul laporan" type="text" class="form-control"></div>
                                    <div class="position-relative form-group">
                                      <label for="exampleSelect" class="">Pilih Kategori</label>
                                      <select name="category_id" class="form-control">
                                        <?php 
                                            $get_category="SELECT * FROM category";
                                            $query_category = mysqli_query($connect,$get_category);
                                            while($row = mysqli_fetch_array($query_category)) {
                                              echo "<option value=".$row['category_id']." >".$row['name']."</option>";
                                            }
                                            ?>
                                      </select>
                                    </div>
                                    <div class="position-relative form-group"><label>Masukkan deskripsi</label><textarea name="description" class="form-control"></textarea></div>
                                    <div class="position-relative form-group"><label>Lampirkan file</label><input name="file" type="file" class="form-control-file">
                                      <small class="form-text text-muted">Masukkan file berformat pdf</small>
                                    </div>
                              </div>
                              <div class="d-block text-center card-footer">
                                  <a href="laporan.php" class="mr-2 btn-icon btn-icon-only btn btn-outline-primary"><i class="pe-7s-back btn-icon-wrapper"> </i></a>
                                  <button type="submit" name="submit" class="btn-wide btn btn-success">Submit</button>
                              </div>
                              <?php
                                if(isset($_POST["submit"])){
                                    $check = $_FILES["file"]["tmp_name"];
                                    if($check !== false){
                                        $generate_laporan_id    = rand(111111,999999);
                                        $user_id                = $_SESSION['user_id'];
                                        $lokasi_file            = $_FILES['file']['tmp_name'];
                                        $nama_file              = $_FILES['file']['name'];
                                        $folder                 = "../files/$user_id.pdf";

                                        $title                 = $_POST['title'];
                                        $category_id           = $_POST['category_id'];
                                        $description          = $_POST['description'];

                                        $create_at      = (new DateTime('now'))->format('Y-m-d H:i:s');
                                        $modified_at    = (new DateTime('now'))->format('Y-m-d H:i:s');
                                        $create_by      = $_SESSION['user_id'];
                                        $modified_by    = $_SESSION['user_id'];
                                        $status         = 'aktif';

                                        if (move_uploaded_file($lokasi_file,"$folder")){
                                          echo "Nama File : <b>$nama_file</b> sukses di upload";

                                          $insert_laporan = "INSERT INTO laporan (laporan_id, user_id, category_id, title, file_laporan, description, status, create_at, modified_at, create_by, modified_by) 
                                          VALUES ('$generate_laporan_id','$user_id',$category_id,'$title','$generate_laporan_id','$description','$status','$create_at','$modified_at','$create_by','$modified_by')";
            
                                          if ($connect-> query($insert_laporan) === TRUE) {
                                              echo "
                                              <script type= 'text/javascript'>
                                                  alert('berhasil menambahkan laporan');
                                                  window.location = 'laporan.php';
                                              </script>";
                                              } else {
                                                  echo "<script type= 'text/javascript'>alert('Error: " . $insert_laporan . "<br>" . $connect->error."');</script>";
                                                  }
                                        }
                                        else{
                                          echo "File gagal di upload";
                                        }

                                    }else{
                                        echo "Please select an pdf file to upload.";
                                    }
                                }
                                ?>
                            </form>
                        </div>
                    </div>
                </div>



                

              </div>
              <?php include("components/footer.php") ?>
            </div>
          <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        </div>
    </div>
  
<script type="text/javascript" src="../core-themes/assets/scripts/main.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body>
</html>
