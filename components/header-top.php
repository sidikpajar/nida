<div class="app-header header-shadow bg-night-sky header-text-light">
            <div class="app-header__logo">
                <div style="color:white;">DIREKTORAT ANGKUTAN UDARA</div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>    
            <div class="app-header__content">
                <div class="app-header-left" style="color: white; text-align:center;">
                    SISTEM INFORMASI PELAPORAN PERJALANAN DINAS        
                </div>
            </div>
        </div>    