<?php 
session_start();
if($_SESSION){
    if($_SESSION['user_level']=="pegawai")
    {
        header("Location: pegawai/index.php");
    }
    if($_SESSION['user_level']=="kepalaseksi")
    {
        header("Location: kepalaseksi/index.php");
    }
     if($_SESSION['user_level']=="subdirektorat")
    {
        header("Location: subdirektorat/index.php");
    }
    if($_SESSION['user_level']=="direktur")
    {
        header("Location: direktur/index.php");
    }
}

include('login.php');
include("components/header.php");
?>
<body>
    <div class="app-container app-theme-white body-tabs-shadow">
      <?php include("components/header-top.php") ?>
      <div style="width:80%; margin: 50px auto 0px auto;">
        <!-- Page Title -->
        <div class="app-page-title card" style="background:white !important;">
          <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                  <i class="pe-7s-graph text-success">
                  </i>
                </div>
                <div>
                  Login Aplikasi
                  <div class="page-title-subheading">Masukkan username dan password anda di Form berikut ini:
                  </div>
                </div>
            </div>
          </div>
        </div>

        <!-- form -->
        <div class="main-card mb-3 card">
          <div class="card-body">
            <h5 class="card-title">Login Form</h5>
            <form class="" method="post" action="">
              <div class="position-relative form-group"><label for="exampleEmail11" class="">NIP</label><input name="user_name" id="exampleEmail11" placeholder="Masukkan NIP Anda" type="text" class="form-control"></div>
              <div class="position-relative form-group"><label for="examplePassword11" class="">Password</label><input name="user_password" id="examplePassword11" placeholder="Password" type="password" class="form-control"></div>
              <button type="submit" name="submit" value="Login" class="mt-2 btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
<script type="text/javascript" src="core-themes/assets/scripts/main.js"></script>
</body>
</html>
