<?php 
  include("components/header.php");
?>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
      <?php include("components/header-top.php") ?>     
      <?php include("components/ui-setting.php") ?>     
        <div class="app-main">
          <?php include("components/menu-sidebar.php") ?>     
            <div class="app-main__outer">
              <!-- main content -->
              <div class="app-main__inner">
                <!-- Title configurasi -->
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon">
                                <i class="pe-7s-car icon-gradient bg-mean-fruit">
                                </i>
                            </div>
                            <div>Sistem Informasi Pelaporan Perjalanan Dinas Direktorat Angkutan Udara
                                <div class="page-title-subheading">Dashboard ini berisi data yang dapat dilihat sesuai dengan hak akses pengguna yang login.
                                </div>
                            </div>
                        </div>
                        <!-- No Page title action -->  
                    </div>
                </div>            
                <div class="row">
                    <div class="col-md-6">
                        <div class="main-card mb-3 card">
                           
                            <div class="card-header">Change password
                            </div>
                            <form class="" role="form" method="POST" action="change-password.php" enctype="multipart/form-data">
                              <div class="col-md-12" style="padding:20px;">
                                  <div class="position-relative form-group"><label>Masukkan password baru</label><input name="user_password" placeholder="Masukkan password baru" type="text" class="form-control"></div>
                              </div>
                              <div class="d-block text-center card-footer">
                                  <a href="laporan.php" class="mr-2 btn-icon btn-icon-only btn btn-outline-primary"><i class="pe-7s-back btn-icon-wrapper"> </i></a>
                                  <button type="submit" name="submit" class="btn-wide btn btn-success">Submit</button>
                              </div>
                              <?php
                                if(isset($_POST["submit"])) {
                                  $user_password    = $_POST['user_password'];
                                  $user_id          = $_SESSION['user_id'];
            
                                  $sql = "UPDATE users SET
                                  user_password='$user_password' WHERE user_id = '$user_id' ";
            
                                    if ($connect-> query($sql) === TRUE ) {
                                    echo "
                                    <script type= 'text/javascript'>
                                        alert('Password Berhasil diUbah');
                                        window.location = 'change-password.php ';
                                    </script>";
            
                                    } else {
                                    echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                                    }
                                    $connect->close();
                                    }
                                ?>
                            </form>
                        </div>
                    </div>
                </div>



                

              </div>
              <?php include("components/footer.php") ?>
            </div>
          <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        </div>
    </div>
  
<script type="text/javascript" src="../core-themes/assets/scripts/main.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body>
</html>
