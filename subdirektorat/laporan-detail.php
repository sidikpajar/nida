<?php 
  include("components/header.php");
  $laporan_id = $_GET['laporan_id'];
?>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
      <?php include("components/header-top.php") ?>     
      <?php include("components/ui-setting.php") ?>     
        <div class="app-main">
          <?php include("components/menu-sidebar.php") ?>     
            <div class="app-main__outer">
              <!-- main content -->
              <div class="app-main__inner">
                <!-- Title configurasi -->
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon">
                                <i class="pe-7s-car icon-gradient bg-mean-fruit">
                                </i>
                            </div>
                            <div>Sistem Informasi Pelaporan Perjalanan Dinas Direktorat Angkutan Udara
                                <div class="page-title-subheading">Dashboard ini berisi data yang dapat dilihat sesuai dengan hak akses pengguna yang login.
                                </div>
                            </div>
                        </div>
                        <!-- No Page title action -->  
                    </div>
                </div>            
                <div class="row">
                    <div class="col-md-5">
                        <div class="main-card mb-3 card">
                            <div class="card-header">Detail laporan
                            </div>
                            <div class="col-md-12" style="padding:20px;">
                            <?php 
                              $get_laporan_detail="SELECT 
                              c.category_id AS category_id,
                              c.name AS category_name,
                              l.laporan_id AS laporan_id,
                              l.user_id AS user_id,
                              l.description AS description,
                              l.title AS title 
                              FROM laporan l
                              INNER JOIN category c 
                                ON c.category_id = l.category_id
                              where laporan_id='$laporan_id' ";
                              $query_laporan = mysqli_query($connect,$get_laporan_detail);
                              while($laporan = mysqli_fetch_array($query_laporan)) {
                            ?>
                              <div class="position-relative form-group">
                                <label><b>Judul</b></label>
                                <p><label><?php echo $laporan['title']; ?></label></p>
                              </div>
                              <div class="position-relative form-group">
                                <label><b>Kategori</b></label>
                                <p><label><?php echo $laporan['category_name']; ?></label></p>
                              </div>
                              <div class="position-relative form-group">
                                <label><b>Deskripsi</b></label>
                                <p><label><?php echo $laporan['description']; ?></label></p>
                              </div>
                              <div class="position-relative form-group">
                                <label><b>Download File Lampiran</b></label>
                                <p><label><a href="../files/<?php echo $laporan['file_laporan']; ?>.pdf" download>Lampiran</a></label></p>
                              </div>
                            <?php } ?>
                            </div>
                            <div class="d-block text-center card-footer">
                                <a href="laporan.php" class="mr-2 btn-icon btn-icon-only btn btn-outline-primary"><i class="pe-7s-back btn-icon-wrapper"> </i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="main-card mb-3 card">
                            <div class="card-header">Logs laporan
                            </div>
                            <div class="col-md-12" style="padding:20px;">
                            <?php 
                             $logs = "SELECT 
                                          u.name AS NAMAUSER,
                                          u.user_level AS LEVELUSER,
                                          r.read_date AS TANGGALDIBACA
                                      FROM read_logs r
                                      INNER JOIN users u
                                        ON u.user_id = r.user_id
                                      where laporan_id='$laporan_id'";
                              $query_logs = mysqli_query($connect,$logs);
                              while($log = mysqli_fetch_array($query_logs)) {
                            ?>
                              <div class="position-relative form-group">
                                  <div class="badge badge-success mr-1 ml-0">
                                      <?php echo $log['LEVELUSER'];?>
                                  </div>
                                  <b style="color:#3f6ad8;"><?php echo $log['NAMAUSER'];?></b> telah membaca tanggal <b><?php echo $log['TANGGALDIBACA'];?></b>
                              </div>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>



                

              </div>
              <?php include("components/footer.php") ?>
            </div>
          <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        </div>
    </div>
  
<script type="text/javascript" src="../core-themes/assets/scripts/main.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body>
</html>
