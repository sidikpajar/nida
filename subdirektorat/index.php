<?php 
  include("components/header.php");
?>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
      <?php include("components/header-top.php") ?>     
      <?php include("components/ui-setting.php") ?>     
        <div class="app-main">
          <?php include("components/menu-sidebar.php") ?>     
            <div class="app-main__outer">
              <!-- main content -->
              <div class="app-main__inner">
                <!-- Title configurasi -->
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon">
                                <i class="pe-7s-car icon-gradient bg-mean-fruit">
                                </i>
                            </div>
                            <div>Sistem Informasi Pelaporan Perjalanan Dinas Direktorat Angkutan Udara
                                <div class="page-title-subheading">Dashboard ini berisi data yang dapat dilihat sesuai dengan hak akses pengguna yang login.
                                </div>
                            </div>
                        </div>
                        <?php include("components/page-title-action.php") ?>
                        <!-- No Page title action -->  
                    </div>
                </div>            
                <div class="row">
                    <div class="col-md-6">
                      <div class="main-card mb-3 card">
                          <div class="card-header">Data pengguna
                          </div>
                          <div class="col-md-12" style="padding:20px;">
                          <?php 
                            $user_id = $_SESSION['user_id'];
                            $get_user_data="SELECT * FROM users where user_id='$user_id' ";
                            $get_user = mysqli_query($connect,$get_user_data);
                            while($user = mysqli_fetch_array($get_user)) {
                          ?>
                            <div class="position-relative form-group">
                              <label><b>NIP</b></label>
                              <p><?php echo $user['user_id']; ?></p>
                            </div>
                            <div class="position-relative form-group">
                              <label><b>Nama</b></label>
                              <p><?php echo $user['name']; ?></p>
                            </div>
                            <div class="position-relative form-group">
                              <label><b>Level</b></label>
                              <p style="text-transform:uppercase;"><?php echo $user['user_level']; ?></p>
                            </div>
                            <div class="position-relative form-group">
                              <label><b>Golongan</b></label>
                              <p>
                                <?php 
                                  $golongan_id = $user['golongan_id'];
                                  $get_golongan="SELECT * FROM golongan where golongan_id='$golongan_id' ";
                                  $golongan = mysqli_query($connect,$get_golongan);
                                  while($data = mysqli_fetch_array($golongan)) {
                                    echo "".$data['pangkat']." - ".$data['golongan']." - ".$data['ruang']."";
                                  }
                                ?>
                              </p>
                            </div>
                            <div class="position-relative form-group">
                              <label><b>Jabatan</b></label>
                              <p>
                                <?php 
                                  $jabatan_id = $user['jabatan_id'];
                                  $get_jabatan="SELECT * FROM jabatan where jabatan_id='$jabatan_id' ";
                                  $jabatan = mysqli_query($connect,$get_jabatan);
                                  while($data_jabatan = mysqli_fetch_array($jabatan)) {
                                ?>
                                  <?php 
                                    if($data_jabatan['parent_id'] !== ''){
                                      $parent_id = $data_jabatan['parent_id'];
                                      $get_parent="SELECT * FROM jabatan where jabatan_id='$parent_id' ";
                                      $parent = mysqli_query($connect,$get_parent);
                                      while($data_parent = mysqli_fetch_array($parent)) {
                                        echo $data_parent['name'];
                                      }
                                      echo "<br/>";
                                      echo $data_jabatan['name'];
                                    } else {
                                      echo $data_jabatan['name'];
                                    }
                                  ?>
                                <?php  } ?>
                              </p>
                            </div>
                            
                          <?php } ?>
                          </div>
                        </div>
                    </div>
                </div>

              </div>
              <?php include("components/footer.php") ?>
            </div>
          <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        </div>
    </div>
<script type="text/javascript" src="../core-themes/assets/scripts/main.js"></script></body>
</html>
