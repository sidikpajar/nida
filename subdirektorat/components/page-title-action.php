<div class="page-title-actions">
    <div class="d-inline-block dropdown">
        <?php     
          $user_id = $_SESSION['user_id'];
          $laporan="SELECT * FROM laporan ";
          $query_laporan = mysqli_query($connect,$laporan);
          $tot = 0;
          $number = 1;
          while($row_laporan = mysqli_fetch_array($query_laporan)) {
            $laporan_id = $row_laporan['laporan_id'];
            $read_logs="SELECT * FROM read_logs WHERE user_id='$user_id' AND laporan_id='$laporan_id' ";
            $query_read_logs = mysqli_query($connect,$read_logs);
            if( ! mysqli_num_rows($query_read_logs) ) {
              $tot = $tot + $number;
            } 
          }
        ?>
        <?php
         if($tot == 0){
          $btn = 'success';
         } else {
          $btn = 'danger';
         }
        ?>
        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-<?php echo $btn?>">
            <span class="btn-icon-wrapper pr-2 opacity-7">
                <i class="fa fa-business-time fa-w-20"></i>
            </span>
            Notifikasi:
            <b>
              <?php  echo $tot;?>
            </b>
            Laporan belum dibaca
        </button>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="laporan.php" class="nav-link">
                        <i class="nav-link-icon lnr-inbox"></i>
                        <span>
                            Laporan
                        </span>
                        <div class="ml-auto badge badge-pill badge-secondary">
                          <?php
                            $sql_total_laporan="SELECT count(title) AS TOTAL FROM laporan";
                            $query = mysqli_query($connect,$sql_total_laporan);
                            while($row = mysqli_fetch_array($query)) {
                            echo $row['TOTAL'];
                            }
                          ?>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="laporan.php" class="nav-link">
                        <i class="nav-link-icon lnr-book"></i>
                        <span>
                          Laporan belum dibaca
                        </span>
                        <div class="ml-auto badge badge-pill badge-<?php echo $btn?>">
                          <?php 
                            echo $tot;
                          ?>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>  