<?php 
  $url = $_SERVER['REQUEST_URI'];
?>
<div class="app-sidebar sidebar-shadow">
   <div class="app-header__logo">
      <div class="logo-src"></div>
      <div class="header__pane ml-auto">
         <div>
            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
            <span class="hamburger-box">
            <span class="hamburger-inner"></span>
            </span>
            </button>
         </div>
      </div>
   </div>
   <div class="app-header__mobile-menu">
      <div>
         <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
         <span class="hamburger-box">
         <span class="hamburger-inner"></span>
         </span>
         </button>
      </div>
   </div>
   <div class="app-header__menu">
      <span>
      <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
      <span class="btn-icon-wrapper">
      <i class="fa fa-ellipsis-v fa-w-6"></i>
      </span>
      </button>
      </span>
   </div>
   <div class="scrollbar-sidebar">
      <div class="app-sidebar__inner">
         <ul class="vertical-nav-menu">
            <li class="app-sidebar__heading">Dashboards</li>
            <li>
              <?php 
                if($url === 'index.php'){
                  echo '<a href="index.php" class="mm-active">';
                } else {
                  echo '<a href="index.php">';
                }
              ?>
                <i class="metismenu-icon pe-7s-rocket"></i>
                Dashboard
               </a>
            </li>
            <li class="app-sidebar__heading">Menu Utama</li>
            <li>
              <?php 
                if($url === 'laporan.php' || $url === 'laporan-add.php'){
                  echo '<a href="laporan.php" class="mm-active">';
                } else {
                  echo '<a href="laporan.php">';
                }
              ?>
                <i class="metismenu-icon pe-7s-pendrive"></i>
                Daftar Laporan
               </a>
            </li>
            <li>
               <a href="change-password.php">
               <i class="metismenu-icon pe-7s-eyedropper"></i>
               Ubah Password
               </a>
            </li>
            <li class="app-sidebar__heading">Pengaturan</li>
            <li>
               <a href="logout.php">
               <i class="metismenu-icon pe-7s-close-circle">
               </i>
               Logout
               </a>
            </li>
         </ul>
      </div>
   </div>
</div>