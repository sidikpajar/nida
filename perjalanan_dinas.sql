-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 14, 2020 at 02:59 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perjalanan_dinas`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` varchar(33) NOT NULL,
  `name` text NOT NULL,
  `status` text NOT NULL,
  `create_at` datetime NOT NULL,
  `create_by` text NOT NULL,
  `modified_at` datetime NOT NULL,
  `modified_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`, `status`, `create_at`, `create_by`, `modified_at`, `modified_by`) VALUES
('1', 'Perjalanan Dinas', 'aktif', '2020-07-10 00:06:12', 'system', '2020-07-10 00:06:12', 'system'),
('2', 'Menghadiri Rapat', 'aktif', '2020-07-10 00:06:12', 'system', '2020-07-10 00:06:12', 'system');

-- --------------------------------------------------------

--
-- Table structure for table `golongan`
--

CREATE TABLE `golongan` (
  `golongan_id` varchar(33) NOT NULL,
  `pangkat` text NOT NULL,
  `golongan` text NOT NULL,
  `ruang` text NOT NULL,
  `status` text NOT NULL,
  `create_at` datetime NOT NULL,
  `create_by` text NOT NULL,
  `modified_at` datetime NOT NULL,
  `modified_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `golongan`
--

INSERT INTO `golongan` (`golongan_id`, `pangkat`, `golongan`, `ruang`, `status`, `create_at`, `create_by`, `modified_at`, `modified_by`) VALUES
('JURU', 'JURU', 'I', 'c', 'aktif', '2020-07-07 20:59:51', 'system', '2020-07-09 20:59:51', 'system\r\n'),
('JURUMUDA', 'JURU MUDA', 'I', 'a', 'aktif', '2020-07-07 20:59:51', 'system', '2020-07-09 20:59:51', 'system\r\n'),
('JURUMUDATKI', 'JURU MUDA TK I', 'I', 'b', 'aktif', '2020-07-07 20:59:51', 'system', '2020-07-09 20:59:51', 'system\r\n'),
('JURUTK.I', 'JURU TK. I', 'I', 'd', 'aktif', '2020-07-07 20:59:51', 'system', '2020-07-09 20:59:51', 'system\r\n'),
('PEMBINA', 'PEMBINA', 'IV', 'a', 'aktif', '2020-07-09 20:36:42', 'system', '2020-07-09 20:36:42', 'system'),
('PEMBINATKI', 'PEMBINA TK. I', 'IV', 'b', 'aktif', '2020-07-09 20:40:39', 'system', '2020-07-09 20:40:39', 'system'),
('PEMBINAUTAMA', 'PEMBINA UTAMA', 'IV', 'e', 'aktif', '2020-07-09 20:53:49', 'system', '2020-07-09 20:53:49', 'system'),
('PEMBINAUTAMAMADY', 'PEMBINA UTAMA MADY', 'IV', 'd', 'aktif', '2020-07-09 20:53:49', 'system', '2020-07-09 20:53:49', 'system'),
('PEMBINAUTAMAMUDA', 'PEMBINA UTAMA MUDA', 'IV', 'c', 'aktif', '2020-07-09 20:40:39', 'system', '2020-07-09 20:40:39', 'system'),
('PENATA', 'PENATA', 'III', 'c', 'aktif', '2020-07-07 20:59:51', 'system', '2020-07-09 20:59:51', 'system\r\n'),
('PENATAMUDA', 'PENATA MUDA', 'III', 'a', 'aktif', '2020-07-07 20:59:51', 'system', '2020-07-09 20:59:51', 'system\r\n'),
('PENATAMUDATK.I', 'PENATA MUDA TK. I', 'III', 'b', 'aktif', '2020-07-07 20:59:51', 'system', '2020-07-09 20:59:51', 'system\r\n'),
('PENATATK.I', 'PENATA TK. I', 'III', 'd', 'aktif', '2020-07-07 20:59:51', 'system', '2020-07-09 20:59:51', 'system\r\n'),
('PENGATUR', 'PENGATUR', 'II', 'c', 'aktif', '2020-07-07 20:59:51', 'system', '2020-07-09 20:59:51', 'system\r\n'),
('PENGATURMUDA', 'PENGATUR MUDA', 'II', 'a', 'aktif', '2020-07-07 20:59:51', 'system', '2020-07-09 20:59:51', 'system\r\n'),
('PENGATURMUDATK.I', 'PENGATUR MUDA TK. I', 'II', 'b\r\n', 'aktif', '2020-07-07 20:59:51', 'system', '2020-07-09 20:59:51', 'system\r\n'),
('PENGATURTK.I', 'PENGATUR TK. I', 'II', 'd', 'aktif', '2020-07-07 20:59:51', 'system', '2020-07-09 20:59:51', 'system\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `jabatan_id` varchar(33) NOT NULL,
  `parent_id` varchar(33) NOT NULL,
  `name` text NOT NULL,
  `status` text NOT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `create_by` text NOT NULL,
  `modified_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`jabatan_id`, `parent_id`, `name`, `status`, `create_at`, `modified_at`, `create_by`, `modified_by`) VALUES
('1', '', 'INSPEKTUR ANGKUTAN UDARA LEVEL V', 'aktif', '2020-07-09 21:20:34', '2020-07-09 21:20:34', 'system', 'system\r\n'),
('10', '', 'KASI ANGKUTAN UDARA NIAGA TDK BERJADWAL DAN BUKAN NIAGA DN', 'aktif', '2020-07-09 21:20:34', '2020-07-09 21:20:34', 'system', 'system\r\n'),
('11', '', 'KASUBDIT ANGKUTAN UDARA NIAGA BERJADWAL', 'aktif', '2020-07-09 21:25:23', '2020-07-09 21:25:23', 'system', 'system\r\n'),
('12', '', 'KASUBDIT PEMBINAAN PENGUSAHAAN DAN TARIF ANGKUTAN UDARA', 'aktif', '2020-07-09 21:25:23', '2020-07-09 21:25:23', 'system', 'system'),
('13', '', 'DIREKTUR ANGKUTAN UDARA', 'aktif', '2020-07-09 21:26:31', '2020-07-09 21:26:31', 'system', 'system'),
('14', '', 'KASI  PENGUSAHAAN ANGKUTAN UDARA', 'aktif', '2020-07-09 21:35:17', '2020-07-09 21:35:17', 'system', 'system'),
('15', '14', 'SUBDIT PEMBINAAN PENGUSAHAAN DAN TARIF ANGKUTAN UDARA', 'aktif', '2020-07-09 21:35:17', '2020-07-09 21:35:17', 'system', 'system'),
('2', '1', 'SEKSI ANGKUTAN UDARA NIAGA BERJADWAL LUAR NEGERI', 'aktif', '2020-07-09 21:20:34', '2020-07-09 21:20:34', 'system', 'system\r\n'),
('3', '1', 'SEKSI TARIF  ANGKUTAN UDARA', 'aktif', '2020-07-09 21:20:34', '2020-07-09 21:20:34', 'system', 'system\r\n'),
('5', '1', 'SEKSI ANGKUTAN UDARA NIAGA BERJADWAL LUAR NEGERI', 'aktif', '2020-07-09 21:20:34', '2020-07-09 21:20:34', 'system', 'system\r\n'),
('6', '5', 'SUBDIT PEMBINAAN PENGUSAHAAN DAN TARIF ANGKUTAN UDARA', 'aktif', '2020-07-09 21:20:34', '2020-07-09 21:20:34', 'system', 'system\r\n'),
('7', '', 'KASI ANGKUTAN UDARA NIAGA TIDAK BERJADWAL  DAN BUKAN NIAGA LN', 'aktif', '2020-07-09 21:20:34', '2020-07-09 21:20:34', 'system', 'system\r\n'),
('8', '7', 'SUBDIT ANGKUTAN UDARA NIAGA TDK BERJADWAL DAN BUKAN NIAGA', 'aktif', '2020-07-09 21:20:34', '2020-07-09 21:20:34', 'system', 'system\r\n'),
('9', '10', 'SUBDIT ANGKUTAN UDARA NIAGA TDK BERJADWAL DAN BUKAN NIAGA', 'aktif', '2020-07-09 21:20:34', '2020-07-09 21:20:34', 'system', 'system\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `laporan`
--

CREATE TABLE `laporan` (
  `laporan_id` varchar(33) NOT NULL,
  `user_id` varchar(33) NOT NULL,
  `category_id` varchar(33) NOT NULL,
  `title` text NOT NULL,
  `file_laporan` text NOT NULL,
  `description` text NOT NULL,
  `status` text NOT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `create_by` text NOT NULL,
  `modified_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan`
--

INSERT INTO `laporan` (`laporan_id`, `user_id`, `category_id`, `title`, `file_laporan`, `description`, `status`, `create_at`, `modified_at`, `create_by`, `modified_by`) VALUES
('1', '197409161998032001', '1', 'Perjalanan dinas dalam rangka peningkatan skill [Part I]', '1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'aktif', '2020-07-10 00:07:20', '2020-07-10 00:07:20', 'system', 'system'),
('2', '197409161998032001', '1', 'Perjalanan dinas dalam rangka peningkatan skill [Part II]\r\n', '2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'aktif', '2020-07-10 00:07:20', '2020-07-10 00:07:20', 'system', 'system'),
('924713', '197612261998032002', '2', 'judul laporan', '924713', 'laporan', 'aktif', '2020-07-11 04:28:13', '2020-07-11 04:28:13', '197612261998032002', '197612261998032002');

-- --------------------------------------------------------

--
-- Table structure for table `read_logs`
--

CREATE TABLE `read_logs` (
  `id` varchar(33) NOT NULL,
  `laporan_id` varchar(33) NOT NULL,
  `user_id` varchar(33) NOT NULL,
  `read_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `read_logs`
--

INSERT INTO `read_logs` (`id`, `laporan_id`, `user_id`, `read_date`) VALUES
('1', '1', '196409071994032001', '2020-07-10 00:26:58'),
('2', '1', '197108151997032001', '2020-07-10 00:26:58'),
('299185', '924713', '197403051999032001', '2020-07-11 05:01:49'),
('417537', '2', '197403051999032001', '2020-07-11 04:54:25'),
('521303', '1', '197403051999032001', '2020-07-11 04:48:56'),
('729545', '924713', '196409071994032001', '2020-07-13 16:11:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` varchar(33) NOT NULL,
  `name` text NOT NULL,
  `user_level` text NOT NULL,
  `user_name` text NOT NULL,
  `user_password` text NOT NULL,
  `golongan_id` varchar(33) NOT NULL,
  `jabatan_id` varchar(33) NOT NULL,
  `status` text NOT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `create_by` text NOT NULL,
  `modified_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `user_level`, `user_name`, `user_password`, `golongan_id`, `jabatan_id`, `status`, `create_at`, `modified_at`, `create_by`, `modified_by`) VALUES
('196409071994032001', 'MARIA KRISTI ENDAH MURNI, SH MH', 'direktur', '196409071994032001', '196409071994032001', 'PEMBINAUTAMAMUDA', '13', 'aktif', '2020-07-09 20:35:22', '2020-07-09 20:35:22', 'system', 'system'),
('197108151997032001', 'URIP RAHAYU, SE  MT', 'subdirektorat', '197108151997032001', '197108151997032001', 'PEMBINATKI', '11', 'aktif', '2020-07-09 20:35:22', '2020-07-09 20:35:22', 'system', 'system'),
('197403051999032001', 'ELLI SETYOWATI, SE MS', 'kepalaseksi', '197403051999032001', '197403051999032001', 'PEMBINA', '15', 'aktif', '2020-07-09 21:33:31', '2020-07-09 21:33:31', 'system', 'system'),
('197409161998032001', 'ERVINA HUTAGALUNG, S Sos . MMTr', 'pegawai', '197409161998032001', '197409161998032001', 'PEMBINA', '2', 'aktif', '2020-07-09 20:35:22', '2020-07-09 20:35:22', 'system', 'system'),
('197612261998032002', 'CHRISTINE DEVIYANTI, SE . MSc', 'pegawai', '197612261998032002', '197612261998032002', 'PEMBINA', '3', 'aktif', '2020-07-09 20:35:22', '2020-07-09 20:35:22', 'system', 'system');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `golongan`
--
ALTER TABLE `golongan`
  ADD PRIMARY KEY (`golongan_id`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`jabatan_id`);

--
-- Indexes for table `laporan`
--
ALTER TABLE `laporan`
  ADD PRIMARY KEY (`laporan_id`);

--
-- Indexes for table `read_logs`
--
ALTER TABLE `read_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
